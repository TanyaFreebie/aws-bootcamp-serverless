import { aws, Schema, model } from 'dynamoose';

/* If we are on local - connect to local table */
// if (process.env.STAGE === 'local') {
    aws.ddb.local(process.env.DYNAMO_URL);
// }

/* Create the schema - how the table data should look) */
const  OrderSchema = new Schema({
  "id": { "type": Number, required: true, "hashKey": true },
  "petId": { "type": Number },
  "quantity": { "type": Number },
  "shipDate": { "type": Date },
  "status": {
    "type": String,
    required: false,
    index: {
      name: "GlobalStatus",
      global: true,
    },
    "rangeKey": true },
  "complete": { "type": Boolean },
});

/* verify that all the env variables are set in .env file */
if (!process || !process.env || !process.env.ORDERS_TABLE_NAME) {
  throw new Error('no Orders table name configured');
}
if (!process || !process.env || !process.env.SERVICE) {
  throw new Error('no service name configured');
}
if (!process || !process.env || !process.env.USERNAME) {
  throw new Error('no username name configured');
}

// const stage = process.env?.STAGE;
const service = process.env?.SERVICE;
const username = process.env?.USERNAME;
const table = process.env?.ORDERS_TABLE_NAME;

/* this is how we access the data in the table, by providing table name and schema */
const Order = model(`${service}-local-${username}-${table}`, OrderSchema);

export default Order;