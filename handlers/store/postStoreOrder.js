import Order from "../../utils/dynamo/orders";

/* this method attempts to create a user in the database based on sent in data */
export default async (event) => {
  try {
    //first we try to convert the body that the caller has sent to us in to an object
    //event.body before parsing is just a text string
    //body is a javacript object
    const body = JSON.parse(event.body);

    // validate that the provided status is correct
    if (body.status !== "available" && body.status !== "pending" && body.status !== "sold" ) {
      return {
        statusCode: 400,
        body: "status should be one of available, pending or sold"
      };
    }
//amount of milliseconds since 1970 Jan 1 00:00:00 GMT
    body.shipDate = (new Date(body.shipDate)).getTime();

    //attempt to create the user in the table, but we need to wait for it's execution
    await Order.create(body);

    // along side with the transformed body
    return {
      statusCode: 200,
      body: JSON.stringify(body),
    };
  } catch (error) {
    // if there are errors along the way with parsing the body
    // inform the user about the issue
    console.error(error);
    return {
      statusCode: 400,
      body: "please check the data you've sent to me"
    };
  }
};