import Order from "../../utils/dynamo/orders";
/* Returns a map of status codes to quantities */
export default async (event) => {
    try {
        const pending = Order.query("status").eq("pending").count().exec();
        const available = Order.query("status").eq("available").count().exec();
        const sold = Order.query("status").eq("sold").count().exec();
        //send response
        const response = await Promise.all([pending, available, sold])
            .then(([resolvedPending, resolvedAvailable, resoledSold]) => {
                return {
                    pending: resolvedPending?.count,
                    available: resolvedAvailable?.count,
                    sold: resoledSold?.count
                };
            });
        return {
            statusCode: 200,
            body: JSON.stringify(response),
        };
    } catch (error) {
        console.error(error);
        return {
          statusCode: 500,
          headers: {},
          body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
          isBase64Encoded: false
        };
    }
};