import Order from "../../utils/dynamo/orders";

export default async (event) => {
  try {
    /* first validate that the order number in path was given correctly - as a number */
    const orderId = Number(event.pathParameters.orderId);
    if (Number.isNaN(orderId) || orderId < 0) {
        /* if order number is not a number, then send error back to user */
        return {
          statusCode: 400,
          body: "Invalid ID supplied",
        };
    }

    /* try to get order from table by id orderId*/
    const order = await Order.query("id").eq(orderId).exec();
    console.log(JSON.stringify(order));
    if (order === undefined) {
        /* no order found in table */
        return {
            statusCode: 404,
           headers: {},
           body: "No Order found",
           isBase64Encoded: false
       };

    }
    Order.delete({"id": orderId}, (error, req) => {
        if (error) {
            console.log("did I got here?");
            console.error(error);
        } else {
            console.log("Successfully deleted item");
            console.error(req);
        }
    });
    // Order.delete( { "id" : orderId } , res => { console.log('ok'); } );

    //   /* all was good, let's send the order back to the user */
    // return {
    //     statusCode: 200,
    //     headers: {},
    //     body: ("Successfully deleted item"),
    //     isBase64Encoded: false
    //   };

    } catch (error) {
    /* something terrible has happened */
    console.error(error);
    return {
      statusCode: 500,
      headers: {},
      body: "Oups, a wild error appeared! Our developers are scrambling to fix this!",
      isBase64Encoded: false
    };
  }
};