export default async (event) => {
    return {
        statusCode: 501,
        body: JSON.stringify({
            message: "This is service not yet implemented",
            path: "/user",
            method: "post"
        })
    };
};